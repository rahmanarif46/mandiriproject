//
//  ArticleListEntity.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 4/08/23.
//

import Foundation

struct ArticlesResponse: Codable {
    var articles: [Article]
}

// MARK: - Article
struct Article: Codable {
    var source: ArticleSource
    var author: String?
    var title: String
    var description: String?
    var url: String
    var urlToImage: String?
    var publishedAt: String
    var content: String?
}

// MARK: - Source
struct ArticleSource: Codable {
    var id: String?
    var name: String
}
