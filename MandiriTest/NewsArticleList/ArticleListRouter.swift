//
//  ArticleListRouter.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 4/08/23.
//

import Foundation

protocol NewsArticleRouterProtocol {
    init(viewController: NewsArticleViewController)
    func pushToDetail(article: Article)
}

class NewsArticleRouter: NewsArticleRouterProtocol {
    unowned let viewController: NewsArticleViewController
        
    required init(viewController: NewsArticleViewController) {
        self.viewController = viewController
    }
    
    func pushToDetail(article: Article) {
        let detailVC = DetailViewController()
        detailVC.article = article
        
        viewController.navigationController?.pushViewController(detailVC, animated: true)
    }
}
