//
//  Extension+UIColor.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 08/08/23.
//

import Foundation
import UIKit

extension UIColor {
    static func random() -> UIColor {
        return UIColor(
            red: .random(),
            green: .random(),
            blue: .random(),
            alpha: 1.0
        )
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random())
    }
}

