//
//  SourceListBuilder.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 4/08/23.
//

import Foundation
import UIKit

class NewsSourceBuilder: BuildModuleProtocol {
    static func build() -> UINavigationController {
        let viewController: SourceListViewController = SourceListViewController()
        let presenter = NewsSourcePresenter()
        let interactor = NewsSourceInteractor()
        let router = NewsSourceRouter(viewController: viewController)
        
        viewController.presenter = presenter
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController
        
        interactor.presenter = presenter
        
        return UINavigationController(rootViewController: viewController)
    }
}
