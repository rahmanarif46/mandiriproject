//
//  SourceListRouter.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 4/08/23.
//

import Foundation

protocol NewsSourceRouterProtocol {
    init(viewController: SourceListViewController)
    func pushToDetail(source: Source)
}

class NewsSourceRouter: NewsSourceRouterProtocol {
    unowned let viewController: SourceListViewController
        
    required init(viewController: SourceListViewController) {
        self.viewController = viewController
    }
    
    func pushToDetail(source: Source) {
        let vc = NewsArticleViewController()
        let presenter = NewsArticlePresenter()
        let interactor = NewsArticleInteractor()
        let router = NewsArticleRouter(viewController: vc)
        
        vc.presenter = presenter
        
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = vc
        
        interactor.presenter = presenter
        
        vc.source = source
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}
