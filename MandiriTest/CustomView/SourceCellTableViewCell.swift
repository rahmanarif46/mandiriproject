//
//  SourceCellTableViewCell.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 10/08/23.
//

import UIKit

class SourceCellTableViewCell: UITableViewCell {

    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(source: Source) {
        titleLabel.text = source.name
        subTitleLabel.text = source.description
        urlLabel.text = source.url
    }
    
}
