//
//  BuildModule.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 07/08/23.
//

import Foundation
import UIKit

protocol BuildModuleProtocol {
    static func build() -> UINavigationController
}
