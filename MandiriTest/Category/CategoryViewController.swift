//
//  CategoryViewController.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 07/08/23.
//

import UIKit

protocol CatagoryViewProtocol: AnyObject {
    func showCategory()
}

class CategoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: CategoryPresenterInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
    }
    
    func initTableView() {
        title = "News API"
        presenter.fetchCategory()
        setupTableView()
        
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.registerCell(type: CategoryTableViewCell.self, identifier: CategoryTableViewCell.identifier)
    }
}

extension CategoryViewController: CatagoryViewProtocol {
    func showCategory() {
        tableView.reloadData()
    }
}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(withType: CategoryTableViewCell.self, for: indexPath) as! CategoryTableViewCell
        
        let category = presenter.category(at: indexPath)
        
        cell.set(category: category)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.pushToListSource(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

