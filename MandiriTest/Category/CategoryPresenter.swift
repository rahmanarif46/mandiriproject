//
//  CategoryPresenter.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 08/08/23.
//

import Foundation

protocol CategoryPresenterInput: AnyObject {
    func fetchCategory()
    func count() -> Int
    func category(at indexPath: IndexPath) -> CategoryItem
    func pushToListSource(at indexPath: IndexPath)
}

protocol CategoryPresenterOutput: AnyObject {
    func presentCategory(item: [CategoryItem])
    func presentToListSource(_ category: CategoryItem)
}

class CategoryPresenter: CategoryPresenterInput {
    
    weak var view: CatagoryViewProtocol!
    var interactor: CategoryInteractorProtocol!
    var router: CatagoryRouterProtocol!
    
    func fetchCategory() {
        interactor.initData()
    }
    
    func count() -> Int {
        interactor.itemCount()
    }
    
    func category(at indexPath: IndexPath) -> CategoryItem {
        interactor.categoryItem(at: indexPath)
    }
    
    func pushToListSource(at indexPath: IndexPath) {
        interactor.pushToSourceList(at: indexPath)
    }
    
}

extension CategoryPresenter: CategoryPresenterOutput {
    func presentCategory(item: [CategoryItem]) {
        view.showCategory()
    }
    
    func presentToListSource(_ category: CategoryItem) {
        router.pushToSource(catagory: category)
    }
}
