//
//  CategoryEntity.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 07/08/23.
//

import Foundation
import UIKit

struct CategoryItem {
    let name: String?
    let color: UIColor?
    
    init(name: String?, color: UIColor?) {
        self.name = name
        self.color = color
    }
}
