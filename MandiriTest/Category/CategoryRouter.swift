//
//  CategoryRouter.swift
//  MandiriTest
//
//  Created by Arif Rahman Sidik on 07/08/23.
//

import Foundation

protocol CatagoryRouterProtocol {
    init(viewController: CategoryViewController)
    func pushToSource(catagory: CategoryItem)
}

class CatagoryRouter: CatagoryRouterProtocol {
    unowned let viewController: CategoryViewController
        
    required init(viewController: CategoryViewController) {
        self.viewController = viewController
    }
    
    func pushToSource(catagory: CategoryItem) {
        let sourceVC = SourceListViewController()
        let presenter = NewsSourcePresenter()
        let interactor = NewsSourceInteractor()
        let router = NewsSourceRouter(viewController: sourceVC)

        sourceVC.presenter = presenter

        presenter.interactor = interactor
        presenter.router = router
        presenter.view = sourceVC

        interactor.presenter = presenter
        sourceVC.item = catagory

        viewController.navigationController?.pushViewController(sourceVC, animated: true)
    }
}
